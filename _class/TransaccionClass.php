<?php
/**
 * Created by PhpStorm.
 * User: Andres
 * Date: 17/06/14
 * Time: 10:00 AM
 */

include_once 'DataBaseClass.php';

class TransaccionClass
{
    //--------------------------------------------------------
    // Atributos
    //--------------------------------------------------------

    /**
     * Numero cuenta de salida
     */
    private $cuentaSalida;

    /**
     * Numero cuenta de entrada
     */
    private $cuentaLlegada;

    /**
     * Monto de la transaccion
     */
    private $monto;

    /**
     * Aprobacion de la transaccion
     */
    private $aprobada;

    /**
     * Codigo transaccion
     */
    private $codigo;

    private $fecha;

    //--------------------------------------------------------
    // Constructor
    //--------------------------------------------------------

    /**
     * Constructor de la clase
     */
    function __construct()
    {
    }

    //--------------------------------------------------------
    // Getter and Setters
    //--------------------------------------------------------

    /**
     * Getter de los atributos
     * @param $property - el nombre del atributo
     * @return mixed - el valor del atributo
     */
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    /**
     * Setter de los atributos
     * @param $property - el nombre del atributo
     * @param $value - el nuevo valor
     */
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    //--------------------------------------------------------
    // Métodos Estaticos
    //--------------------------------------------------------

    /**
     * @param $preparedSentence mysqli_stmt
     * @return array
     */
    private static function getAList($preparedSentence){
        $cuenta_salida = $cuenta_llegada = $monto = $aprobada = $codigo_transaccion = $tiempo_transaccion = null;
        $transacciones = array();
		if($preparedSentence->execute()){
            $preparedSentence->bind_result($cuenta_salida,$cuenta_llegada,$monto,$aprobada,$codigo_transaccion,$tiempo_transaccion);
            while($preparedSentence->fetch())
            {
                $transaccion = new TransaccionClass();
                $transaccion->asignarDatosTransaccion($cuenta_salida,$cuenta_llegada,$monto,$aprobada,$codigo_transaccion,$tiempo_transaccion);
                array_push($transacciones,$transaccion);
            }
        }
        $preparedSentence->close();
        return $transacciones;
    }

    /**
     * @param $preparedSentence mysqli_stmt
     * @return null|TransaccionClass
     */
    private static function getAnElement($preparedSentence){
        $cuenta_salida = $cuenta_llegada = $monto = $aprobada = $codigo_transaccion = $tiempo_transaccion = null;
        $transaccion = null;
        if($preparedSentence->execute())
        {
            $preparedSentence->bind_result($cuenta_salida,$cuenta_llegada,$monto,$aprobada,$codigo_transaccion,$tiempo_transaccion);
            if($preparedSentence->fetch()){
                $transaccion = new TransaccionClass();
                $transaccion->asignarDatosTransaccion($cuenta_salida,$cuenta_llegada,$monto,$aprobada,$codigo_transaccion,$tiempo_transaccion);
            }
        }
        $preparedSentence->close();
        return $transaccion;
    }

    public static function lista(){
        $conexion = DataBaseClass::darConexion();
        $stmt = $conexion->prepare("SELECT CUENTA_SALIDA,CUENTA_LLEGADA,MONTO,APROBADA,CODIGO_TRANSACCION,TIEMPO_TRANSACCION FROM SecureCoding.TRANSACCIONES");
        return TransaccionClass::getAList($stmt);
    }

    public static function darTransaccionesPorCliente($login){
        $conexion = DataBaseClass::darConexion();
        $stmt = $conexion->prepare("SELECT CUENTA_SALIDA,CUENTA_LLEGADA,MONTO,APROBADA,CODIGO_TRANSACCION,TIEMPO_TRANSACCION FROM SecureCoding.TRANSACCIONES join (SELECT NUMERO_CUENTA, LOGIN FROM CUENTAS join CLIENTES on LOGIN=login_usuario) as T1 on NUMERO_CUENTA = CUENTA_SALIDA or NUMERO_CUENTA = CUENTA_LLEGADA WHERE LOGIN = ?");
        $stmt->bind_param("s",$login);
        return TransaccionClass::getAList($stmt);
    }

    public static function darTransaccionesNoAprobadas(){
        $conexion = DataBaseClass::darConexion();
        $stmt = $conexion->prepare("SELECT CUENTA_SALIDA,CUENTA_LLEGADA,MONTO,APROBADA,CODIGO_TRANSACCION,TIEMPO_TRANSACCION FROM SecureCoding.TRANSACCIONES WHERE APROBADA=0");
        return TransaccionClass::getAList($stmt);
    }

    public static function darTransaccionesPorCodigo($codigo){
        $conexion = DataBaseClass::darConexion();
        $stmt = $conexion->prepare("SELECT CUENTA_SALIDA,CUENTA_LLEGADA,MONTO,APROBADA,CODIGO_TRANSACCION,TIEMPO_TRANSACCION FROM SecureCoding.TRANSACCIONES WHERE CODIGO_TRANSACCION=?");
        $stmt->bind_param("s",$codigo);
        return TransaccionClass::getAnElement($stmt);
    }

    public static function rechazarTransaccion($codigo){
        $conexion = DataBaseClass::darConexion();
        $stmt = $conexion->prepare("UPDATE SecureCoding.TRANSACCIONES SET APROBADA = 2 WHERE CODIGO_TRANSACCION = ?");
        $stmt->bind_param("s",$codigo);
        if($stmt->execute()){
            return true;
        }
        return false;
    }

    //--------------------------------------------------------
    // Métodos
    //--------------------------------------------------------

    /**
     * Inicializa los datos de la carrera
     * @param $cuenta_salida
     * @param $cuenta_llegada
     * @param $monto
     * @param $aprobada int
     * @param $codigo
     * @param $fecha
     */
    public function asignarDatosTransaccion($cuenta_salida, $cuenta_llegada, $monto, $aprobada, $codigo, $fecha)
    {
        $this->cuentaSalida=$cuenta_salida;
        $this->cuentaLlegada=$cuenta_llegada;
        $this->monto=$monto;
        $this->aprobada=$aprobada;
        $this->codigo=$codigo;
        $this->fecha=$fecha;
    }

    public function aprobarTransaccion(){
        if($this->aprobada==0 && $this->cuentaLlegada!=$this->cuentaSalida){
            $conexion = DataBaseClass::darConexion();
            $stmt = $conexion->prepare("UPDATE SecureCoding.TRANSACCIONES SET APROBADA = 1 WHERE CODIGO_TRANSACCION = ?");
            $stmt->bind_param("s",$this->codigo);
            if($stmt->execute()){
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * Guarda una transaccion en la base de datos.
     * @return bool - True si fue posible guardar la carrera, false de lo contrario.
     */
    public function save()
    {
        $conexion = DataBaseClass::darConexion();
        $transaccion = TransaccionClass::darTransaccionesPorCodigo($this->codigo);
        if(!isset($transaccion)){
            $stmt = $conexion->prepare("INSERT INTO SecureCoding.TRANSACCIONES (CUENTA_SALIDA, CUENTA_LLEGADA, MONTO, APROBADA, CODIGO_TRANSACCION) VALUES (?,?,?,?,?)");
            $stmt->bind_param("iiiis",$this->cuentaSalida,$this->cuentaLlegada,$this->monto,$this->aprobada,hash('sha512',$this->codigo));
            if($stmt->execute()){
                return true;
            }
            return false;
        }
    }
}