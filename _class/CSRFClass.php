<?php
/**
 * Created by PhpStorm.
 * User: Andres
 * Date: 7/07/14
 * Time: 07:42 PM
 */

class CSRFClass {

    public static function  request_token_generate( $data_str, $key, $timeout = 900 ) {
        $now = microtime( true );
        $hash = hash_hmac( 'sha256', "$data_str|$now|$timeout", $key );
        return base64_encode( $hash ) . "|$now|$timeout";
    }

    public static function request_token_verify( $token, $data_str, $key ) {
        list( $hash, $hash_time, $timeout ) = explode( '|', $token, 3 );
        if ( empty( $hash ) || empty( $hash_time ) || empty( $timeout ) ) {
            return false;
        }

        if ( microtime( true ) > $hash_time + $timeout ) {
            return false;
        }

        $hash = base64_decode( $hash );
        $check_hash = hash_hmac(
            'sha256', "$data_str|$hash_time|$timeout", $key
        );

        if ( $check_hash === $hash ) {
            return true;
        }

        return false;
    }

} 