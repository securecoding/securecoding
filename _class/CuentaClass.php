<?php
/**
 * Created by PhpStorm.
 * User: Andres
 * Date: 17/06/14
 * Time: 11:23 AM
 */

include_once 'DataBaseClass.php';

class CuentaClass{

    //--------------------------------------------------------
    // Atributos
    //--------------------------------------------------------

    /**
     * El login del usuario de la cuenta
     */
    private $login_usuario;

    /**
     * Numero de la cuenta
     */
    private $numero_cuenta;

    /**
     * Saldo de la cuenta
     */
    private $saldo;

    //--------------------------------------------------------
    // Constructor
    //--------------------------------------------------------

    /**
     * Constructor de la clase
     */
    function __construct()
    {
    }

    //--------------------------------------------------------
    // Getter and Setters
    //--------------------------------------------------------

    /**
     * Getter de los atributos
     * @param $property - el nombre del atributo
     * @return mixed - el valor del atributo
     */
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    /**
     * Setter de los atributos
     * @param $property - el nombre del atributo
     * @param $value - el nuevo valor
     */
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    //--------------------------------------------------------
    // Métodos Estaticos
    //--------------------------------------------------------

    /**
     * @param $preparedSentence mysqli_stmt
     * @return array
     */
    private static function getAList($preparedSentence){
        $cuentas = array();
        $login_usuario = $numero_cuenta = $saldo = null;
        if($preparedSentence->execute()){
            $preparedSentence->bind_result($login_usuario,$numero_cuenta,$saldo);
            while($preparedSentence->fetch())
            {
                $cuenta = new CuentaClass();
                $cuenta->asignarDatosCuenta($login_usuario,$numero_cuenta,$saldo);
                array_push($cuentas,$cuenta);
            }
        }
        $preparedSentence->close();
        return $cuentas;
    }

    /**
     * @param $preparedSentence mysqli_stmt
     * @return CuentaClass|null
     */
    private static function getAnElement($preparedSentence){
        $cuenta = null;
        $login_usuario = $numero_cuenta = $saldo = null;
        if($preparedSentence->execute())
        {
            $preparedSentence->bind_result($login_usuario,$numero_cuenta,$saldo);
            if($preparedSentence->fetch()){
                $cuenta = new CuentaClass();
                $cuenta->asignarDatosCuenta($login_usuario,$numero_cuenta,$saldo);
            }
        }
        return $cuenta;
    }

    public static function lista(){
        $conexion = DataBaseClass::darConexion();
        $stmt = $conexion->prepare("SELECT LOGIN_USUARIO,NUMERO_CUENTA,SALDO FROM SecureCoding.CUENTAS");
        return CuentaClass::getAList($stmt);
    }

    public static function darCuentasPorLogin($login){
        $conexion = DataBaseClass::darConexion();
        $stmt = $conexion->prepare("SELECT LOGIN_USUARIO,NUMERO_CUENTA,SALDO FROM SecureCoding.CUENTAS WHERE LOGIN_USUARIO=?");
        $stmt->bind_param("s",$login);
        return CuentaClass::getAList($stmt);
    }

    public static function darCuentaPorNumero($num){
        $conexion = DataBaseClass::darConexion();
        $stmt = $conexion->prepare("SELECT LOGIN_USUARIO,NUMERO_CUENTA,SALDO FROM SecureCoding.CUENTAS WHERE NUMERO_CUENTA=?");
        $stmt->bind_param("i",$num);
        return CuentaClass::getAnElement($stmt);
    }

    //--------------------------------------------------------
    // Métodos
    //--------------------------------------------------------

    /**
     * Inicializa los datos de la carrera
     * @param $login_usuario
     * @param $saldo
     */
    public function asignarDatos($login_usuario, $saldo)
    {
        $this->login_usuario=$login_usuario;
        $this->saldo=$saldo;
    }

    /**
     * Inicializa los datos de la carrera
     * @param $login_usuario
     * @param $numero_cuenta
     * @param $saldo
     */
    private function asignarDatosCuenta($login_usuario, $numero_cuenta, $saldo)
    {
        $this->login_usuario=$login_usuario;
        $this->numero_cuenta=$numero_cuenta;
        $this->saldo=$saldo;
    }

    public function aumentarSaldo($saldo){
        $this->saldo+=$saldo;
        return $this->save();
    }

    public function disminuirSaldo($saldo){
        if($this->saldo-$saldo>=0){
            $this->saldo-=$saldo;
            return $this->save();
        }
        return false;
    }

    /**
     * Guarda un funcionario nuevo en la base de datos.
     * @return bool - True si fue posible guardar el funcionario, false de lo contrario.
     */
    public function save()
    {
        $conexion = DataBaseClass::darConexion();
        if(!isset($this->numero_cuenta)){
            $stmt = $conexion->prepare("INSERT INTO SecureCoding.CUENTAS (LOGIN_USUARIO, NUMERO_CUENTA, SALDO) VALUES (?, ?, ?)");
            $stmt->bind_param("sii",$this->login_usuario,$this->numero_cuenta,$this->saldo);
            if($stmt->execute()){
                $numero =null;
                $stmt2 = $conexion->prepare("SELECT MAX(NUMERO_CUENTA) AS id FROM CUENTAS");
                $stmt2->bind_result($numero);
                if ($stmt2->execute())
                {
                    if($stmt2->fetch()){
                        $this->numero_cuenta = $numero;
                        return true;
                    }
                }
            }
            return false;
        }
        else{
            $stmt = $conexion->prepare("UPDATE SecureCoding.CUENTAS SET SALDO=? WHERE NUMERO_CUENTA=?");
            $stmt->bind_param("ii",$this->saldo,$this->numero_cuenta);
            if($stmt->execute()){
                return true;
            }
            return false;
        }
    }
} 