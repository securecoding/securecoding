<?php
/**
 * Created by PhpStorm.
 * User: Andres
 * Date: 17/06/14
 * Time: 10:00 AM
 */

include_once 'DataBaseClass.php';

class FuncionarioClass{
    //--------------------------------------------------------
    // Atributos
    //--------------------------------------------------------

    /**
     * El login del funcionario
     */
    private $login;

    /**
     * El pass del funcionario
     */
    private $pass;

    /**
     * Nombre del funcionario
     */
    private $nombre;

    /**
     * Apellido del funcionario
     */
    private $apellido;

    //--------------------------------------------------------
    // Constructor
    //--------------------------------------------------------

    /**
     * Constructor de la clase
     */
    function __construct()
    {
    }

    //--------------------------------------------------------
    // Getter and Setters
    //--------------------------------------------------------

    /**
     * Getter de los atributos
     * @param $property - el nombre del atributo
     * @return mixed - el valor del atributo
     */
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    /**
     * Setter de los atributos
     * @param $property - el nombre del atributo
     * @param $value - el nuevo valor
     */
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    //--------------------------------------------------------
    // Métodos Estaticos
    //--------------------------------------------------------

    /**
     * @param $preparedSentence mysqli_stmt
     * @return array
     */
    private static function getAList($preparedSentence){
        $login = $password = $nombre = $apellido = null;
        $funcionarios = array();
        if($preparedSentence->execute()){
            $preparedSentence->bind_result($login,$password,$nombre,$apellido);
            while($preparedSentence->fetch())
            {
                $funcionario = new FuncionarioClass();
                $funcionario->asignarDatosFuncionario($login,$password,$nombre,$apellido);
                array_push($funcionarios,$funcionario);
            }
        }
        $preparedSentence->close();
        return $funcionarios;
    }

    /**
     * @param $preparedSentence mysqli_stmt
     * @return FuncionarioClass|null
     */
    private static function getAnElement($preparedSentence){
        $login = $password = $nombre = $apellido = null;
        $funcionario = null;
        if($preparedSentence->execute())
        {
            $preparedSentence->bind_result($login,$password,$nombre,$apellido);
            if($preparedSentence->fetch()){
                $funcionario = new FuncionarioClass();
                $funcionario->asignarDatosFuncionario($login,$password,$nombre,$apellido);
            }
        }
        $preparedSentence->close();
        return $funcionario;
    }

    public static function lista(){
        $conexion = DataBaseClass::darConexion();
        $stmt = $conexion->prepare("SELECT LOGIN,PASSWORD,NOMBRE,APELLIDO FROM SecureCoding.FUNCIONARIOS");
        return FuncionarioClass::getAList($stmt);
    }

    public static function darFuncionarioPorLogin($login){
        $conexion = DataBaseClass::darConexion();
        $stmt = $conexion->prepare("SELECT LOGIN,PASSWORD,NOMBRE,APELLIDO FROM SecureCoding.FUNCIONARIOS WHERE LOGIN=?");
        $stmt->bind_param("s",$login);
        return FuncionarioClass::getAnElement($stmt);
    }

    //--------------------------------------------------------
    // Métodos
    //--------------------------------------------------------

    /**
     * Inicializa los datos de la carrera
     * @param $login String
     * @param $pass String
     * @param $nombre String
     * @param $apellido String
     */
    public function asignarDatosFuncionario($login, $pass, $nombre, $apellido)
    {
        $this->login=$login;
        $this->pass=$pass;
        $this->nombre=$nombre;
        $this->apellido=$apellido;
    }

    /**
     * Verifica si $password coincide con la contraseña del usuario
     * @param $password - la constraseña a analizar
     * @return bool - true si $password coincide con la contraseña del usuario false de lo contrario.
     */
    public function verificarPassword($password)
    {
        if(hash('sha512',$password) == $this->pass){
            return true;
        }
        return false;
    }

    /**
     * Guarda un funcionario nuevo en la base de datos.
     * @return bool - True si fue posible guardar el funcionario, false de lo contrario.
     */
    public function save()
    {
        $conexion = DataBaseClass::darConexion();
        if(FuncionarioClass::darFuncionarioPorLogin($this->login)==null){
            $stmt = $conexion->prepare("INSERT INTO SecureCoding.FUNCIONARIOS (LOGIN,PASSWORD,NOMBRE,APELLIDO) VALUES ('$this->login','".hash('sha512',$this->pass)."',
                '$this->nombre','$this->apellido')");
            $stmt->bind_param("sssssss",$this->login,hash('sha512',$this->pass),$this->nombre,$this->apellido);
            if($stmt->execute()){
                return true;
            }
            return false;
        }
        else{
            $stmt = $conexion->prepare("UPDATE SecureCoding.FUNCIONARIOS SET NOMBRE=?, APELLIDO = ?");
            $stmt->bind_param("ss",$this->nombre,$this->apellido);
            if($stmt->execute()){
                return true;
            }
            return false;
        }
    }
}