<?php
include_once("_bl/_verify_employee_session.php");
include_once("_templates/head.php");
$employee = FuncionarioClass::darFuncionarioPorLogin($_SESSION["_SecureCodingEmployeeSessionID_"]);
?>
<body>
    <div id="page">
		<?php include_once("_templates/header.php") ?>
	</div>
	<div id="content">
		<div id="container">
			<div id="main">
				<?php include_once("_templates/employee/employee_menu.php") ?>
				<div id="text">
					<h1>Perfil</h1>
					<p>
						<table>
							<tr>
								<td>Nombre</td>
								<td><?php echo $employee->nombre." ".$employee->apellido?></td>
							</tr>
						</table>
					</p>
				</div>
			</div>
		</div>
		<?php include_once("_templates/footer.php") ?>
	</div> 
	</body>
</html>
