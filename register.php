﻿<?php
include_once("_templates/head.php") ?>
<body>
    <div id="page">
		<?php include_once("_templates/header.php") ?>
	</div>
	<div id="content">
		<div id="container">
			<div id="main">
				<?php include_once("_templates/general_menu.php") ?>
				<div id="text">
					<h1>Registro</h1>
					<p>Debes llenar TODOS los siguientes campos:</p>
						<table>
							<tr>
								<td>Login: </td>
								<td><input name="login" id="login"></td>
							</tr>
							<tr>
								<td>Password: </td>
								<td><input type="password" name="pass" id="pass"></td>
							</tr>
							<tr>
								<td>Confirmar password: </td>
								<td><input type="password" name="repass" id="repass"></td>
							</tr>
							<tr>
								<td>Nombre: </td>
								<td><input name="nombre" id="nombre"></td>
							</tr>
							<tr>
								<td>Apellido: </td>
								<td><input name="apellido" id="apellido"></td>
							</tr>
							<tr>
								<td>Email: </td>
								<td><input name="correo" id="correo"></td>
							</tr>
							<tr>
								<td>Confirmar email: </td>
								<td><input name="recorreo" id="recorreo"></td>
							</tr>
                            <tr>
                                <td>Telefono: </td>
                                <td><input name="telefono" id="telefono"></td>
                            </tr>
                            <tr>
                                <td>Dirección: </td>
                                <td><input name="direccion" id="direccion"></td>
                            </tr>
                            <tr>
                                <td>Que tipo de transacción desea: </td>
                                <td>
                                    <select name="tipo" id="tipo">
                                        <option value="0">Tans (Codigos)</option>
                                        <option value="1">Token </option>
                                    </select>
                                </td>
                            </tr>
							<tr>
								<td>
                                    <button id="register">Registrarse</button>
								</td>
							</tr>
						</table>
						<?php include_once("_templates/message_boxes.php") ?>
				</div>
			</div>
		</div>
		<?php include_once("_templates/footer.php") ?>
		</div> 
	</body>
<script>
    $(document).ready( function(){
        $('#register').click(function(){
            verificarDatosRegistro();
        })
    })
</script>
</html>
