 <p></p>
 <div id="wait" class="msgBox wait" style="width:300px; left:0px;">
	<p align="center">
		<span id="waitText">Mensaje</span>
		<div style="clear: both"></div>
	</p>
</div>

<div id="success" class="msgBox success" style="width:300px; left:0px;">
	<p align="center">
		<img src="_img/icon/success.png">
		<span id="successText">Mensaje</span>
		<div style="clear: both"></div>
	</p>
</div>

<div id="error" class="msgBox error" style="width:300px; left:0px;">
	<p align="center">
		<img src="_img/icon/error.png">
		<span id="errorText">Mensaje</span>
		<div style="clear: both"></div>
	</p>
</div>