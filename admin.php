<?php
session_start();
if(isset($_SESSION["_SecureCodingEmployeeSessionID_"])){
    header("Location: employee.php");
}
include_once("_templates/head.php")
?>
	<body>
		<div id="page">
			<?php include_once("_templates/header.php") ?>
		</div>
		<div id="content">
			<div id="container">
				<div id="main">
					<?php include_once("_templates/general_menu.php") ?>
					<div id="text">
						<h1>Login Empleados</h1>
						<p>Para poder entrar debes hacer login.</p>
							<table>
								<tr>
									<td>Login:</td>
									<td><input id="login"></td>
								</tr>
								<tr>
									<td>Password:</td>
									<td><input id="pass" type="password"></td>
								</tr>
								<tr>
									<td>
										<button id="do_login">Login</button>
									</td>
								</tr>
							</table>
						<?php include_once("_templates/message_boxes.php") ?>
					</div>
				</div>
			</div>
			<?php include_once("_templates/footer.php") ?>
		</div> 
	</body>
<script>
    $(document).ready( function(){
        $('#do_login').click(function(){
            verificarDatosLoginEmployee();
        })
    })
</script>
</html>
