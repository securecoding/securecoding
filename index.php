<?php
session_start();
if(isset($_SESSION["_SecureCodingSessionID_"])){
    header("Location: client.php");
}
include_once("_templates/head.php") ?>
<body>
    <div id="page">
		<?php include_once("_templates/header.php") ?>
	</div>
	<div id="content">
		<div id="container">
			<div id="main">
				<?php include_once("_templates/general_menu.php") ?>
				<div id="text">
					<h1>Login Usuario</h1>
						<table>
							<tr>
								<td>Login:</td>
								<td><input name="login" id="login"></td>
							</tr>
							<tr>
								<td>Password:</td>
								<td><input name="pass" id="pass" type="password"></td>
							</tr>
							<tr>
								<td>
									<button id="do_login">Login</button>
								</td>
							</tr>
						</table>
					<p>Si eres un nuevo usuario registrate <a href="register.php">aqui</a></p>
					<?php include_once("_templates/message_boxes.php") ?>
				</div>
			</div>
		</div>
		<?php include_once("_templates/footer.php") ?>
	</div>
    <script>
        $(document).ready( function(){
            $('#do_login').click(function(){
                verificarDatosLogin();
            })
        })
    </script>
	</body>
</html>