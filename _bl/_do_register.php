<?php
function __autoload($nombre){
    include_once "../_class/$nombre.php";
}
if(isset($_POST["login"]) && isset($_POST["pass"]) && isset($_POST["repass"]) &&
    isset($_POST["nombre"]) && isset($_POST["apellido"]) && isset($_POST["telefono"]) &&
    isset($_POST["direccion"]) && isset($_POST["correo"]) && isset($_POST["recorreo"]) && isset($_POST["tipo"]))
{
    $login = htmlspecialchars($_POST["login"]);
    $pass = htmlspecialchars($_POST["pass"]);
    $repass = htmlspecialchars($_POST["repass"]);
    $nombre = htmlspecialchars($_POST["nombre"]);
    $apellido = htmlspecialchars($_POST["apellido"]);
    $telefono = htmlspecialchars($_POST["telefono"]);
    $direccion = htmlspecialchars($_POST["direccion"]);
    $correo = htmlspecialchars($_POST["correo"]);
    $recorreo = htmlspecialchars($_POST["recorreo"]);
    $tipo = htmlspecialchars($_POST["tipo"]);
    if($pass != $repass){
        echo "Las contraseñas no coinciden";
    }
    elseif ($correo != $correo){
        echo "Los correos no coinciden";
    }
    else{

        //VERIFICACION DE LOS PARAMETROS
        if(!VerificadorClass::validarAlfanumerico($login) ||
            VerificadorClass::validarVacio($login) ||
            VerificadorClass::validarLongitudMenorA($login,6) ||
            VerificadorClass::validarLongitudMayorA($login,30) ||
            VerificadorClass::validarVacio($pass)||
            VerificadorClass::validarLongitudMenorA($pass,6)||
            VerificadorClass::validarVacio($nombre)||
            !VerificadorClass::validarAlfabetico($nombre)||
            VerificadorClass::validarLongitudMenorA($nombre,2)||
            VerificadorClass::validarLongitudMayorA($nombre,45)||
            VerificadorClass::validarVacio($apellido)||
            !VerificadorClass::validarAlfabetico($apellido)||
            VerificadorClass::validarLongitudMenorA($apellido,2)||
            VerificadorClass::validarLongitudMayorA($apellido,45)||
            VerificadorClass::validarVacio($correo)||
            !VerificadorClass::validarCorreo($correo)||
            VerificadorClass::validarVacio($telefono)||
            !VerificadorClass::validarNumerico($telefono)||
            VerificadorClass::validarLongitudMenorA($telefono,7)||
            VerificadorClass::validarLongitudMayorA($telefono,10) ||
            VerificadorClass::validarVacio($direccion)||
            VerificadorClass::validarLongitudMayorA($direccion,45)
        ){
            echo "Error en los parametros";
        }
        //FIN VERIFICACION DE LOS PARAMETROS

        else{
            $cliente = ClienteClass::darClientePorID($login);
            if(isset($cliente)){
                echo "Ya existe un usuario con dicho login";
            }
            else{
                $cliente = new ClienteClass();
                $cliente->asignarDatosCliente($login,$pass,$nombre,$apellido,$telefono,$direccion,$correo,0,$tipo);
                if($cliente->save()){
                    echo("success");
                }
                else{
                    echo "error";
                }
            }
        }
    }
}
else{
    echo "error";
}