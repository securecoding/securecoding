<?php
function __autoload($nombre){
    include_once "../_class/$nombre.php";
}
session_start();
if(!isset($_SESSION["_SecureCodingSessionID_"]) && isset($_POST["login"]) && isset($_POST["pass"])){
    $login = htmlspecialchars($_POST["login"]);
    $pass = htmlspecialchars($_POST["pass"]);

    //VERIFICACION DE LOS PARAMETROS
    if(!VerificadorClass::validarAlfanumerico($login) ||
        VerificadorClass::validarVacio($login) ||
        VerificadorClass::validarLongitudMenorA($login,6) ||
        VerificadorClass::validarLongitudMayorA($login,30) ||
        VerificadorClass::validarLongitudMenorA($pass,6)||
        VerificadorClass::validarVacio($pass)
        ){
        echo "Error en los parametros";
    }
    //FIN VERIFICACION DE LOS PARAMETROS
    else{
        $user = ClienteClass::darClientePorID($login);
        if(isset($user) && $user->verificarPassword($pass)){
            if($user->aprobado ==1){
                $_SESSION["_SecureCodingSessionID_"]=$login;
                echo "success";
            }
            else{
                echo "Tu cuenta no ha sido aprobada";
            }
        }else{
            echo "Error al iniciar sesión. Por favor verifica tus datos.";
        }
    }
}
else{
    echo "Error no identificado";
}