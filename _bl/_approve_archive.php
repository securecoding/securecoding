<?php
function __autoload($nombre){
    include_once "../_class/$nombre.php";
}
include_once("_verify_sesion.php");
if(isset($_POST["cuenta_salida"])&&isset($_POST["numeral"])&& isset($_POST["token"])){

    //VERIFICACION DEL TOKEN
    $token = $_POST["token"];
    $data_str = "approve_archive-".$_SESSION["_SecureCodingSessionID_"];
    $key = hash('sha512', $_SESSION["_SecureCodingSessionID_"]);
    if(CSRFClass::request_token_verify($token,$data_str,$key)){
        $cuentaSalida = htmlspecialchars($_POST["cuenta_salida"]);
        $numeral = htmlspecialchars($_POST["numeral"]);
        $login = $_SESSION["_SecureCodingSessionID_"];

        //VERIFICACION DE LOS PARAMETROS
        if(!VerificadorClass::validarAlfanumerico($login) ||
            VerificadorClass::validarVacio($login) ||
            VerificadorClass::validarLongitudMenorA($login,6) ||
            VerificadorClass::validarLongitudMayorA($login,30) ||
            VerificadorClass::validarVacio($cuentaSalida)||
            !VerificadorClass::validarNumerico($cuentaSalida)||
            VerificadorClass::validarVacio($numeral)||
            !VerificadorClass::validarNumerico($numeral)){
            echo "Error en los parametros";
        }
        //FIN VERIFICACION DE LOS PARAMETROS

        else{
            $cuentas = CuentaClass::darCuentasPorLogin($login);
            //Verificar si la cuenta de salida corresponde a la sesion
            $termino = false;
            for($i=0;$i<count($cuentas) && !$termino;$i++) {
                $cuenta = $cuentas[$i];
                if($cuenta->numero_cuenta == $cuentaSalida){
                    $termino=true;
                }
            }
            if($termino){
                $server = "http://localhost/uploads/";
                $serverPath = "/../../../../usr/local/uploads/";
                // Agrego el archivo al servidor y obtengo la ruta donde queda guardada
                if(isset($_FILES["archive"])){
                    $archivo = $_FILES["archive"]['name'];
                    $prefijo = substr(md5(uniqid(rand())),0,10);
                    $result=explode(".",$archivo,2);
                    $extension = $result[1];
                    $destino =  $serverPath.$prefijo."_.".$extension;
                    $ruta = $prefijo."_.".$extension;
                    //Verificar que solo llegan archivo txt
                    if($extension=="txt"){
                        var_dump($destino);
                        if (!copy($_FILES["archive"]['tmp_name'],$destino)) {
                            header('Location: /client_transaction_error.php');
                        }
                        else{
                            $command = "./script $cuentaSalida $numeral /usr/local/uploads/$ruta";
                            $i = 2;
                            $array = array();
                            exec("chmod 777 /usr/local/uploads/$ruta");
                            $resultado = exec($command,$array,$i);
                            var_dump($resultado);
                            var_dump($array);
                            var_dump("/usr/local/uploads/$ruta");
                            if($i==0){
                                header('Location: /client_transaction_success.php');
                            }
                            else{
                                header('Location: /client_transaction_error.php');
                            }
                        }
                    }
                    else{
                        header('Location: /client_transaction_error.php');
                    }
                }
            }
            else{
                header('Location: /client_transaction_error.php');
            }
        }
    }
    else{
        header('Location: /client_transaction.php');
    }
}
else{
    header('Location: /client_transaction.php');
}