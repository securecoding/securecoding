<?php
function __autoload($nombre){
    include_once "../_class/$nombre.php";
}
include_once("_verify_employee_session.php");
if(isset($_POST["codigo"]) && isset($_POST["token"])){
    $codigo = htmlspecialchars($_POST["codigo"]);

    //VERIFICACION DEL TOKEN
    $token = $_POST["token"];
    $data_str = "approve_transaction-".$_SESSION["_SecureCodingEmployeeSessionID_"];
    $key = hash('sha512', $_SESSION["_SecureCodingEmployeeSessionID_"]);
    if(CSRFClass::request_token_verify($token,$data_str,$key)){
        //VERIFICACION DE LOS PARAMETROS
        if(!VerificadorClass::validarAlfanumerico($codigo) ||
            VerificadorClass::validarVacio($codigo) ||
            VerificadorClass::validarLongitudMenorA($codigo,128) ||
            VerificadorClass::validarLongitudMayorA($codigo,128)){
            echo "Error en los parametros";
        }
        //FIN VERIFICACION DE LOS PARAMETROS
        else{
            $transaccion = TransaccionClass::darTransaccionesPorCodigo($codigo);
            $conexion = DataBaseClass::darConexion();

            //INICIA LA TRANSACCION
            $conexion->autocommit(false);
            if($transaccion->aprobarTransaccion()){
                $cuenta = CuentaClass::darCuentaPorNumero($transaccion->cuentaLlegada);
                if($cuenta->aumentarSaldo($transaccion->monto)){
                    $conexion->commit();
                    $conexion->autocommit(true);
                    $conexion->close();
                    echo "success";
                }
                else{
                    $conexion->rollback();
                    $conexion->autocommit(true);
                    $conexion->close();
                    echo "error";
                }
            }
            else{
                $conexion->rollback();
                $conexion->autocommit(true);
                $conexion->close();
                echo "error";
            }
        }
    }
    else{
        echo "Error de sesión";
    }
}
else{
    echo "error";
}