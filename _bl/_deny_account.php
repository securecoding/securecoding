<?php
function __autoload($nombre){
    include_once "../_class/$nombre.php";
}
include_once("_verify_employee_session.php");
if(isset($_POST["login"]) && isset($_POST["token"])){
    $login = htmlspecialchars($_POST["login"]);

    //VERIFICACION DEL TOKEN
    $token = $_POST["token"];
    $data_str = "deny_account-".$_SESSION["_SecureCodingEmployeeSessionID_"];
    $key = hash('sha512', $_SESSION["_SecureCodingEmployeeSessionID_"]);
    if(CSRFClass::request_token_verify($token,$data_str,$key)){
        //VERIFICACION DE LOS PARAMETROS
        if(!VerificadorClass::validarAlfanumerico($login) ||
            VerificadorClass::validarVacio($login) ||
            VerificadorClass::validarLongitudMenorA($login,6) ||
            VerificadorClass::validarLongitudMayorA($login,30)){
            echo "Error en los parametros";
        }
        //FIN VERIFICACION DE LOS PARAMETROS
        else{
            if(ClienteClass::rechazarRegistro($login)){
                echo "success";
            }
            else{
                echo "Error al intentar rechazar. Por Favor intente mas tarde";
            }
        }
    }
    else{
        echo "Error de sesion";
    }
}
else{
    echo "error";
}