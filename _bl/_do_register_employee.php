<?php
function __autoload($nombre){
    include_once "../_class/$nombre.php";
}
if(isset($_POST["login"]) && isset($_POST["pass"]) && isset($_POST["repass"]) &&
    isset($_POST["nombre"]) && isset($_POST["apellido"]))
{
    $login = htmlspecialchars($_POST["login"]);
    $pass = htmlspecialchars($_POST["pass"]);
    $repass = htmlspecialchars($_POST["repass"]);
    $nombre = htmlspecialchars($_POST["nombre"]);
    $apellido = htmlspecialchars($_POST["apellido"]);
    if($pass != $repass){
        echo "Las contraseñas no coinciden";
    }
    else{

        //VERIFICACION DE LOS PARAMETROS
        if(!VerificadorClass::validarAlfanumerico($login) ||
            VerificadorClass::validarVacio($login) ||
            VerificadorClass::validarLongitudMenorA($login,6) ||
            VerificadorClass::validarLongitudMayorA($login,30) ||
            VerificadorClass::validarVacio($pass)||
            VerificadorClass::validarLongitudMenorA($pass,6)||
            VerificadorClass::validarVacio($nombre)||
            !VerificadorClass::validarAlfabetico($nombre)||
            VerificadorClass::validarLongitudMenorA($nombre,2)||
            VerificadorClass::validarLongitudMayorA($nombre,45)||
            VerificadorClass::validarVacio($apellido)||
            !VerificadorClass::validarAlfabetico($apellido)||
            VerificadorClass::validarLongitudMenorA($apellido,2)||
            VerificadorClass::validarLongitudMayorA($apellido,45)
        ){
            echo "Error en los parametros";
        }
        //FIN VERIFICACION DE LOS PARAMETROS
        else{
            $funcionario = FuncionarioClass::darFuncionarioPorLogin($login);
            if(isset($funcionario)){
                echo "Ya existe un funcionario con dicho login";
            }
            else{
                $funcionario = new FuncionarioClass();
                $funcionario->asignarDatosFuncionario($login,$pass,$nombre,$apellido);
                if($funcionario->save()){
                    echo("success");
                }
                else{
                    echo "Ocurrió un error al registarse. Por favor intente más tarde.";
                }
            }
        }
    }
}
else{
    echo "Error no identificado";
}