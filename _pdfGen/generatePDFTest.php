<?php
	include ('./src/Cezpdf.php');	
	$pdf = new Cezpdf('a4', 'portrait');
	$pdf->tempPath  = './tmp/';
	$pdf->selectFont('FreeSerif','', 1, true);
	$pdf->ezSetCmMargins(2,2.5,2,2);
	$pdf->setEncryption("trees", "", array('copy','print'));
	
	$pdf->ezText("Warrior Bank", 50, array("justification"=>"center"));
	$pdf->ezText("¡Gracias por confiar en nosotros!",
		16,
		array("justification"=>"center", "spacing"=>1.5));
	$pdf->ezText("Aquí se encuentra la lista de los códigos que necesita para aprobar transacciones.",
		14,
		array("spacing"=>1.5, "justification"=>"full"));
	$pdf->ezText("Es importante conservar éstos códigos. Consérvelos en un lugar seguro. Cada vez que se usa un código ya no podrá volver a usarse.",
		14,
		array("spacing"=>1, "justification"=>"full"));
		
	$lastY = $pdf->ezText("   ",
		14,
		array("spacing"=>1));
	
	$codigos = array(
			array('num'=>1,'codigo'=>'gandalf'),
			array('num'=>2,'codigo'=>'bilbo'),
			array('num'=>3,'codigo'=>'frodo'),
			array('num'=>4,'codigo'=>'saruman'),
			array('num'=>5,'codigo'=>'sausdadaron')
	);
	$cols = array('num'=>'No', 'codigo'=>'Código');
	
	$pdf->ezTable($codigos, $cols, 'Lista de códigos', array('width' =>470));
	
	$doc = $pdf->ezOutput();
	
	$pdfFileName = "./tmp/tmpPDf.pdf";
	$fh = fopen($pdfFileName, 'w');
	fwrite($fh, $doc);
	fclose($fh);
	echo("Boom! Done!");
?>