<?php include_once("_templates/head.php") ?>
	<body>
		<div id="page">
			<?php include_once("_templates/header.php") ?>
		</div>
		<div id="content">
			<div id="container">
				<div id="main">
					<?php include_once("_templates/general_menu.php") ?>
					<div id="text">
						<h1>Sesión de usuario cerrada</h1>
						<p>Has cerrado sesión exitosamente</p>	
						
						<p>Volver al <a href="index.php">inicio</a></p>
					</div>
				</div>
				</div>
			</div>
			<?php include_once("_templates/footer.php") ?>
		</div> 
	</body>
</html>
