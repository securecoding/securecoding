<?php
include_once("_templates/head.php") ?>
	<body>
		<div id="page">
			<?php include_once("_templates/header.php") ?>
		</div>
		<div id="content">
			<div id="container">
				<div id="main">
					<?php include_once("_templates/general_menu.php") ?>
					<div id="text">
						<h1>Registro exitoso</h1>
						<p>Felicidades! Te has registrado exitosamente</p>
						
						<div id="clientOnly">
							<p>Debes esperar para que tu cuenta sea aprobada. Cuando asi sea, recibirás un correo con instrucciones de como usar tu cuenta y el modo de hacer transacciones, de acuerdo al tipo de transacción que decidiste usar (TANS o Tokens).</p>
							<p><strong>¡Muchas gracias por registrarse en Warrior Bank!</strong></p>
						</div>
						
						<p>Volver al <a href="index.php">inicio</a></p>
					</div>
				</div>
		</div>
		<?php include_once("_templates/footer.php") ?>
		</div> 
	</body>
</html>
