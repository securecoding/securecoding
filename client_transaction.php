<?php
include_once("_bl/_verify_sesion.php");
include_once("_templates/head.php");
$cliente = ClienteClass::darClientePorID($_SESSION["_SecureCodingSessionID_"]);
?>
<body>
    <div id="page">
		<?php include_once("_templates/header.php") ?>
	</div>
	<div id="content">
		<div id="container">
			<div id="main">
				<?php include_once("_templates/client/client_menu.php") ?>
				<div id="text">
					<h1>Realizar Transacción</h1>
					<p>
						<form>
							<p>
								<input type="radio" name="group1" value="normal" checked onClick="hide_archive_form()">Envio normal de formulario</input><br>
								<input type="radio" name="group1" value="archive" onClick="hide_normal_form()">Envio por archivo</input> 
							</p>
						</form>
							<table  id="normal_form"  style="display:block">
                                <tr>
                                    <td>Numero cuenta salida</td>
                                    <td>
                                        <select id="cuenta_salida">
                                            <?php
                                            $cuentas = CuentaClass::darCuentasPorLogin($cliente->login);
                                            foreach($cuentas as $cuenta){?>
                                            <option value="<?php echo $cuenta->numero_cuenta; ?>">
                                                <?php echo $cuenta->numero_cuenta; ?>
                                            </option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
								<tr>
									<td>Numero cuenta destino</td>
									<td><input id="cuenta_destino"></td>
								</tr>
								<tr>
									<td>Monto</td>
									<td><input id="monto"></td>
								</tr>
								<tr>
								<?php if  ($cliente->tipo_transaccion == 0) { ?>
									<td class="confirmation">Ingrese el código de validación #<?php echo(CodigoClass::darNumeralCodigoValido($cliente->login)); ?></td>
                                    <input id="numeral" type="hidden" value="<?php echo(CodigoClass::darNumeralCodigoValido($cliente->login)); ?>">
									<td><input id="codigo"></td>
								<?php } else if ($cliente->tipo_transaccion == 1) { ?>
									<td class="confirmation">Ingrese el código de validación generado por el programa que le fue entregado a través de su correo. </td>
									<input id="numeral" name="numeral" type="hidden" value="6574309">
									<td><input id="codigo"></td>
								<?php 
								}
                                $data_str = "do_transaction-".$_SESSION["_SecureCodingSessionID_"];
                                $key = hash('sha512', $_SESSION["_SecureCodingSessionID_"]);
								?>
								</tr>
                                <input type="hidden" name="token" id="token" value="<?php echo CSRFClass::request_token_generate($data_str,$key); ?>"/>
								<tr>
									<td>
										<button id="finalizar">Finalizar</button>
									</td>
								</tr>
							</table>
                        <form method="post" enctype="multipart/form-data" action="_bl/_approve_archive.php">
							<table id="archive_form" style="display:none">
                                <tr>
                                    <td>Numero cuenta salida</td>
                                    <td>
                                        <select id="cuenta_salida" name="cuenta_salida">
                                            <?php
                                            $cuentas = CuentaClass::darCuentasPorLogin($cliente->login);
                                            foreach($cuentas as $cuenta){?>
                                                <option value="<?php echo $cuenta->numero_cuenta; ?>">
                                                    <?php echo $cuenta->numero_cuenta; ?>
                                                </option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
								<tr>
									<td>Archivo txt para enviar</td>
									<td><input type="file" name="archive" id="archive"></td>
								</tr>
								<?php if  ($cliente->tipo_transaccion == 0) { ?>
                                <tr>
                                    <td class="confirmation">Ingrese desde el código de validación #<?php echo(CodigoClass::darNumeralCodigoValido($cliente->login)); ?></td>
                                    <input id="numeral" name="numeral" type="hidden" value="<?php echo(CodigoClass::darNumeralCodigoValido($cliente->login)); ?>">
                                </tr>
								<?php } else if ($cliente->tipo_transaccion == 1) { ?>
									<input id="numeral" name="numeral" type="hidden" value="6574309">
								<?php 
								}
                                $data_str = "approve_archive-".$_SESSION["_SecureCodingSessionID_"];
                                $key = hash('sha512', $_SESSION["_SecureCodingSessionID_"]);
								?>
                                <input type="hidden" name="token" id="token" value="<?php echo CSRFClass::request_token_generate($data_str,$key); ?>"/>
								<tr>
									<td>
										<input type="submit" value="Finalizar"/>
									</td>
								</tr>
							</table>
                        </form>
					</p>
					<?php include_once("_templates/message_boxes.php") ?>
				</div>
			</div>
		</div>
		<?php include_once("_templates/footer.php") ?>
		</div> 
	</body>
<script>
    $(document).ready( function(){
        $('#finalizar').click(function(){
            verificarDatosTransaccion();
        })
    })
</script>
</html>
