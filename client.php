<?php
include_once("_bl/_verify_sesion.php");
include_once("_templates/head.php");
$cliente = ClienteClass::darClientePorID($_SESSION["_SecureCodingSessionID_"]);
?>
	<body>
		<div id="page">
			<?php include_once("_templates/header.php") ?>
		</div>
		<div id="content">
			<div id="container">
				<div id="main">
					<?php include_once("_templates/client/client_menu.php") ?>
					<div id="text">
						<h1>Perfil Cliente</h1>
						<p>
							<table width = 500px>
								<tr>
									<td>Nombre</td>
									<td><?php echo $cliente->nombre." ".$cliente->apellido; ?></td>
								</tr>
                            <?php
                            $cuentas = CuentaClass::darCuentasPorLogin($cliente->login);
                            foreach($cuentas as $cuenta){?>
                                <tr>
                                    <td>Numero de cuenta</td>
                                    <td><?php echo $cuenta->numero_cuenta; ?></td>
                                </tr>
                                <tr>
                                    <td>Saldo</td>
                                    <td><?php echo $cuenta->saldo; ?></td>
                                </tr>
                            <?php
                            }
                            ?>
							</table>
						</p>
					</div>
				</div>
			</div>
			<?php include_once("_templates/footer.php") ?>
		</div> 
	</body>
</html>
