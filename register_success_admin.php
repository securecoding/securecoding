﻿<?php
include_once("_templates/head.php") ?>
	<body>
		<div id="page">
			<?php include_once("_templates/header.php") ?>
		</div>
		<div id="content">
			<div id="container">
				<div id="main">
					<?php include_once("_templates/general_menu.php") ?>
					<div id="text">
						<h1>Registro exitoso</h1>
						<p>Felicidades! Te has registrado exitosamente</p>						
						<p>Volver al <a href="admin.php">inicio de empleados</a></p>
					</div>
				</div>
		</div>
		<?php include_once("_templates/footer.php") ?>
		</div> 
	</body>
</html>
