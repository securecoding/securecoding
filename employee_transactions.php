<?php
include_once("_bl/_verify_employee_session.php");
include_once("_templates/head.php");
?>
	<body>
		<div id="page">
			<?php include_once("_templates/header.php") ?>
		</div>
		<div id="content">
			<div id="container">
				<div id="main">
					<?php include_once("_templates/employee/employee_menu.php") ?>
					<div id="text">
						<h1>Aprobar transacciones</h1>
						<p>
							<table style="margin-top: 35px">
								<tr>
									<td>Numero cuenta origen</td>
									<td>Numero cuenta destino</td>
									<td>Monto</td>
									<td>Accion</td>
								</tr>
								<tr>
                                    <?php
                                    $transacciones = TransaccionClass::darTransaccionesNoAprobadas();
                                    foreach($transacciones as $transaccion){?>
                                        <td><?php echo $transaccion->cuentaSalida ?></td>
                                        <td><?php echo $transaccion->cuentaLlegada ?></td>
                                        <td><?php echo $transaccion->monto ?></td>
                                        <td>
                                            <button onclick="aprobarTransaccion('<?php echo($transaccion->codigo); ?>','<?php $data_str = "approve_transaction-".$_SESSION["_SecureCodingEmployeeSessionID_"];
                                            $key = hash('sha512', $_SESSION["_SecureCodingEmployeeSessionID_"]); echo CSRFClass::request_token_generate($data_str,$key); ?>')">Aprobar</button>
                                            <button onclick="rechazarTransaccion('<?php echo($transaccion->codigo); ?>','<?php $data_str = "deny_transaction-".$_SESSION["_SecureCodingEmployeeSessionID_"];
                                            $key = hash('sha512', $_SESSION["_SecureCodingEmployeeSessionID_"]); echo CSRFClass::request_token_generate($data_str,$key); ?>')">Rechazar</button>
                                        </td>
                                    <?php
                                    }
                                    ?>
								</tr>
							</table>
							<?php include_once("_templates/message_boxes.php") ?>
						</p>
					</div>
				</div>
		</div>
		<?php include_once("_templates/footer.php") ?>
		</div> 
	</body>
</html>
